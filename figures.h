const char base_figure[6][8] = {{"======="},    \
                                {"I      "},    \
                                {"I      "},    \
                                {"I      "},    \
                                {"I      "},    \
                                {"I\\    "}};

const char figure[6][5] = {{" O "},             \
                           {" I "},             \
                           {"\\I "},            \
                           {"\\I/"},            \
                           {"/  "},             \
                           {"/ \\"}};


/* This array represents the layers where the strings
   of the current stage(array index) should be printed */
const int stages[] = {0, 1, 1, 1, 2, 2};
