# Hangman
## Compile on Linux
Get the ncurses library (on some distributions e.g. Debian the headers are included in the ncurses-dev package.)

Compile by typing `make`

## Compile on Windows
Download the mingw compiler [here](https://sourceforge.net/projects/mingw/files/Installer/)

Additionally select the pdcurses library.

Otherwise download [pdc34dllw.zip](https://sourceforge.net/projects/pdcurses/files/pdcurses/3.4/)
and put the files into the following mingw directories:

`pdcurses.lib` --> /lib

`curses.h` and `panel.h` --> /include

`pdcures.dll` --> /bin    

Now you can compile the project with `mingw32-make -f makefile_mingw`